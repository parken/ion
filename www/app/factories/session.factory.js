angular.module('starter')
  .factory('Session', function Session($window) {
      var sessionService = {};

      sessionService.create = function create(key, value) {
        $window.localStorage[key] = angular.toJson(value);
      };

      sessionService.read = function read(key) {
        if($window.localStorage[key]){
          try {
            return angular.fromJson($window.localStorage[key]);
          } catch(e){
            return false
          }

        } else {
          return false;
        }

      };

      sessionService.destroy = function destroy() {
        $window.localStorage.clear();
      };

      sessionService.isAuthenticated = function isAuthenticated() {
        var returnValue = !!(sessionService.read('department') && sessionService.read('department').id);
        return returnValue
      };

      sessionService.getAccessToken = function getAccessToken() {
        return sessionService.read('oauth') && sessionService.read('oauth').access_token;
      };

      sessionService.isAuthorized = function isAuthorized(authorizedRoles) {
        var roles = authorizedRoles;
        if (!angular.isArray(roles)) {
          roles = [].push(roles);
        }

        return (sessionService.isAuthenticated() && ~roles.indexOf(sessionService.userRole));
      };

      return sessionService;
    });
