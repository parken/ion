(function () {
  'use strict';
  angular.module('starter').filter('imgur',function(){
    return function(input, type){
      if(input){
        if(type) {
          return input.splice(input.length-4,0,type)
        } else {
          return input.splice(input.length-4,0,"b")
        }

      }
    }

  })
  angular.module('starter').directive('loading', function ($http) {
    return {
      template: '<div></div>',
      restrict: 'A',
      link: function (scope, element, attrs) {
        scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };
        scope.$watch(scope.isLoading, function (v) {
          if (v) {
            element.show();
          } else {
            element.hide();
          }
        });
      }
    };
  });
}).call(this);
