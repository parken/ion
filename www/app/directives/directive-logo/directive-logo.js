/**
 * Created by ManjeshV on 11/06/16.
 */

(function () {
  'use strict';

  angular.module('starter').directive('directiveLogo', function ($http, Session) {
    return {
      template: '<img ng-src="data:{{deparment.Hotel.Chain.Logo.mime}};base64,{{deparment.Hotel.Chain.Logo.logo}}" style="width:170px">',
      link: function (scope, element, attrs) {
        scope.deparment = Session.read('department');
      }
    };
  });
}).call(this);

