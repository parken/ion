'use strict';

angular.module('starter')
  .config(function ($stateProvider) {
    $stateProvider
      .state('tickets', {
        abstract: true,
        url: '/tickets',
        template: '<div ui-view></div>',

      })
      .state('tickets.list', {
        url: '?ticketId',
        templateUrl: 'app/routes/tickets/list/list.html',
        controller: 'TicketsListController',
        controllerAs: 'TicketsList'
      })


  });
