(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('user-login', {
      url: '/user-login',
      templateUrl: 'app/routes/user-login/user-login.html',
      controller: 'UserLoginCtrl'

    });
  });

  'use strict';

  angular.module('starter').controller('UserLoginCtrl', function($scope, Session, $state, Restangular) {

    $scope.$on("$ionicView.beforeEnter", function() {
      $scope.user = {}
    })

    $scope.login = function(user){
      Restangular.all('open/users/login')
        .post(user)
        .then(function(response){
          var userData = response.data
          Session.create('feedback-user',userData);
          return $state.go('hotel-home')
        })
        .catch(function(err) {
          if(err.data && err.data.code === 400){
            alert("Please check password")
          }
          alert("Please check internet connection")
        })


    }
  });

}).call(this);
