(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('department-login', {
      url: '/department-login',
      templateUrl: 'app/routes/department-login/department-login.html',
      controller: 'DepartmentLoginCtrl',


    });
  });

  'use strict';

  angular.module('starter').controller('DepartmentLoginCtrl', function($scope, $ionicHistory, $state, Restangular,Session) {
    $ionicHistory.nextViewOptions({
      disableBack: true
    });

    $scope.$on("$ionicView.beforeEnter", function() {
      $scope.departmentId = '';
      if(Session.read('department')) return  $state.go('home');

      $scope.continue = function(departmentId){
        Restangular.one('open/departments',departmentId).all('login').customGET().then(function(department){
          Session.create('department',department.plain());
          Session.create('departmentId',parseInt(departmentId));
          $state.go('home');
        }).catch(function(err){
          alert("Please check internet connection")
          console.log("Error while login",err)
        })
      }
    })



  });

}).call(this);
