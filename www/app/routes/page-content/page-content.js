(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('page-content', {
      url: '/page-content',
      templateUrl: 'app/routes/page-content/page-content.html',
      controller: 'PageContentCtrl'

    });
  });

  'use strict';

  angular.module('starter').controller('PageContentCtrl', function($scope, $state, $rootScope, Session,$ionicHistory, $ionicScrollDelegate) {

    $scope.$on("$destroy", function() {
      var delegate = $ionicScrollDelegate.$getByHandle('mainScroll');
      delegate. forgetScrollPosition();
    });

    $ionicHistory.nextViewOptions({
      disableBack: true
    });

    $scope.$on('$ionicView.loaded', function(){

      //$ionicScrollDelegate.scrollTo(0,100,false);
    });

    $scope.$on("$ionicView.beforeEnter", function() {
      var department = Session.read('department')

      $scope.currentPageIndex = 0
      $scope.Survey = department.Survey
      $scope.totalPages = department.Survey.Pages.length
      if(!$scope.totalPages){
        return $state.go('summary')
      }

      $scope.contentBtnClick=function(index){
        $scope.opened = ($scope.opened == index) ? -1 : index
      }

      $scope.questionBtnClick=function(index){
        $scope.opened = ($scope.opened == index) ? -1 : index
      }
      var scrollHeight = 0;
      var navigateToNextPage = function(){
        if($scope.totalPages === $scope.currentPageIndex+1){
          Session.create('department_response', $scope.Survey)
          return $state.go('summary')
        }
        $scope.currentPageIndex++
        return $scope.loadPage()
      }

      $scope.loadPage = function(){
        $scope.currentPage = department.Survey.Pages[$scope.currentPageIndex]
        if($scope.currentPage.type== 'content'){
          $scope.totalItems = $scope.currentPage.Contents.length
          //console.log('returning ')
          //$scope.currentPageIndex++
          //return navigateToNextPage()
        } else if($scope.currentPage.type== 'question'){
          $scope.totalItems = $scope.currentPage.Questions.length

        }
        return $scope.opened = 0
      }

      $scope.loadPage()



      var navigateToNextQuestion = function(contentIndex,type){

        if($scope.totalItems === contentIndex+1){
          var flag;
          if($scope.currentPage.type== 'content'){
            flag = (-1 === _.map($scope.currentPage.Contents,'answer').indexOf(undefined))
          }

          if($scope.currentPage.type== 'question'){
            flag = (-1 === _.map($scope.currentPage.Questions,'answerText').indexOf(undefined))
          }

          if(flag){
            $scope.message = ""
            //setTimeout(function(){
              $ionicScrollDelegate.scrollTo(0,0,false);
              scrollHeight = 0;
            //},1000)
            return navigateToNextPage()
          } else {
            $scope.message = "Please check all questions to continue..."
          }

        }

        scrollHeight += type === "content" ? 90 : 100;
        $ionicScrollDelegate.scrollTo(0,contentIndex*90,true);


        return $scope.opened = contentIndex+1
      }

      $scope.answerBtnClick=function(content,value,text,contentIndex){
        content.answer = value
        content.answerText = text
        navigateToNextQuestion(contentIndex,'content')
      }
      $scope.qanswerBtnClick = function(content,value,text,contentIndex){
        content.answer = value
        content.answerText = text
        navigateToNextQuestion(contentIndex, 'question')
      }
    });



  });

}).call(this);
