(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('lineManager-login', {
      url: '/lineManager-login',
      templateUrl: 'app/routes/lineManager-login/lineManager-login.html',
      controller: 'LineManagerLoginCtrl'

    });
  });

  'use strict';

  angular.module('starter').controller('LineManagerLoginCtrl', function($scope, Session, $state, Restangular) {

    $scope.$on("$ionicView.beforeEnter", function() {
      $scope.user = {}

      Restangular.one('open/departments',localStorage.departmentId)
        .customGET('lineManager')
        .then(function(response){
          $scope.userData = response
          localStorage.lineManagerId = response.LineManager.id
          $scope.user.user = response.LineManager.email || response.LineManager.mobile
        })
        .catch(function(err) {
          if(err.data && err.data.code === 400){
            alert("Please check password")
          }
          alert("Please check internet connection")
        })

    })

    $scope.login = function(user){
      Restangular.all('open/users/login')
        .post(user)
        .then(function(response){

          var dept = Session.read('department');


          return $state.go('owner-dashboard-survey', {
            chainId:dept.Hotel.Chain.id,
            hotelId: dept.Hotel.id,
            surveyId:dept.Survey.id,
            departmentId:dept.id
          })

        })
        .catch(function(err) {
          if(err.data && err.data.code === 400){
            return alert("Please check password")
          }
          alert("Please check internet connection")
        })


    }
  });

}).call(this);
