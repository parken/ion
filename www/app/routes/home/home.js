(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('home', {
      url: '/home',
      templateUrl: 'app/routes/home/home.html',
      controller: 'HomeCtrl',
      //resolve: {
      //  loggedIn: function($state,Session){
      //    console.log(!Session.read('department'),Session.read('department'))
      //    if(!Session.read('department')){
      //      console.log('not logged in')
      //      console.log($state.go('department-login'))
      //      return  0
      //    }
      //  }
      //}
    });
  });

  'use strict';

  angular.module('starter').controller('HomeCtrl', function($scope, $state, $ionicHistory, $ionicSideMenuDelegate, Session) {


    $scope.toggleRight = function() {
      $ionicSideMenuDelegate.toggleRight();
    };

    $ionicHistory.nextViewOptions({
      disableBack: true
    });
    $scope.rightButton ='<img class="title-image" src="assets/beatle_logo.png" />';
    $scope.changeDepartment = function(){
      localStorage.removeItem('department')
      localStorage.removeItem('departmentId')
      $state.go('department-login')
    }
  });

}).call(this);
