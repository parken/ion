(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('my-reports', {
      url: '/my-reports',
      templateUrl: 'app/routes/my-reports/my-reports.html',
      controller: 'MyReportsCtrl'

    });
  });

  'use strict';

  angular.module('starter').controller('MyReportsCtrl', function(Restangular,$scope,Session) {


    $scope.$on("$ionicView.beforeEnter", function() {
      var departmentId = Session.read('departmentId')
      Restangular.one('open/departments',departmentId).customGET('feedbacks/count',{})
        .then(function(response){
          $scope.count = response.count
        })
    });


  });

}).call(this);
