(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('user-signup', {
      url: '/user-signup',
      templateUrl: 'app/routes/user-signup/user-signup.html',
      controller: 'UserSignupCtrl'

    });
  });

  'use strict';

  angular.module('starter').controller('UserSignupCtrl', function($scope, Session, $state,Restangular ) {

    $scope.$on("$ionicView.beforeEnter", function() {
      $scope.user = {}
    })

    $scope.signup = function(user,signupForm){
      if(user.password === user.rPassword){
        Restangular.all('open/users/signup')
          .post(user)
          .then(function(response) {
            Session.create('feedback-user',response.data);
            return $state.go('hotel-home')
          })
          .catch(function(err) {
            alert("Please check internet connection")
          })
      } else {
        signupForm.password.$setValidity('required',false)
        signupForm.rpassword.$setValidity('required',false)
      }

    }

  });

}).call(this);
