'use strict';

angular.module('starter')
  .config(function ($stateProvider) {
    $stateProvider
      .state('notifications', {
        abstract: true,
        url: '/notifications',
        template: '<div ui-view></div>',

      })
      .state('notifications.list', {
        url: '?notificationsId',
        templateUrl: 'app/routes/tickets/list/list.html',
        controller: 'NotificationsListController',
        controllerAs: 'NotificationsList'
      })


  });
