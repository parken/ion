(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('hotel-home', {
      url: '/hotel-home',
      templateUrl: 'app/routes/hotel-home/hotel-home.html',
      controller: 'HotelHomeCtrl'

    });
  });

  'use strict';

  angular.module('starter').controller('HotelHomeCtrl', function($scope,Session,$state) {

    var fullInfo = Session.read('department')
    $scope.hotelName= fullInfo.Hotel.name




    $scope.signout = function(){
      localStorage.removeItem('feedback-user')
      $state.go('home')
    }

  });

}).call(this);
