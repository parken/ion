(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('summary', {
      url: '/summary',
      templateUrl: 'app/routes/summary/summary.html',
      controller: 'SummaryCtrl'

    });
  });

  angular.module('starter').controller('SummaryCtrl', function(Session,$scope,$ionicHistory,Restangular,$state) {

    $ionicHistory.nextViewOptions({
      disableBack: true
    });


    $scope.$on("$ionicView.beforeEnter", function() {

      document.getElementById('myImage').src = ""

      $scope.xcamera = false;

      if(navigator.camera){
        $scope.xcamera = true;
      } else {
        $scope.xcamera = false;
      }

      $scope.getPhoto = function(){
        if(navigator.camera){
          navigator.camera.getPicture(onSuccess, onFail, {
            quality: 100,
            destinationType: Camera.DestinationType.DATA_URL,
            cameraDirection: Camera.Direction.FRONT,
            correctOrientation: true
          });
        }
      }
      $scope.imageFlag = false;
      function onSuccess(imageData) {
        var image = document.getElementById('myImage');
        image.src = "data:image/jpeg;base64," + imageData;
        $scope.image = image.src;
        $scope.imageFlag = true;
      }

      function onFail(message) {
        $scope.message = "Unable to take photo"
      }

      $scope.Department = Session.read('department')
      $scope.Survey = Session.read('department_response')
      $scope.user = Session.read('feedback-user')
      //$scope.userIdType = isNaN($scope.user.name) ? 'Email':'Mobile'

      $scope.submit = function(){
        var sum = Session.read('department_response');
        // sum.image = $scope.image;
        // if(navigator.camera){
        //   if(!sum.image){
        //     return $scope.errorMessage = "Please take phone"
        //   }
        // }

        sum.surveyId = sum.id;
        delete sum.id;

        sum.hotelName = $scope.Department.Hotel.name;
        sum.chainName = $scope.Department.Hotel.Chain.name;
        sum.mobile = $scope.user.mobile;
        sum.email = $scope.user.email;
        sum.userId = $scope.user.id;
        sum.username = $scope.user.name;
        sum.departmentId = Session.read('departmentId');

        Restangular.all('open/feedbacks/v1').post(sum).then(function(){
            delete $scope.image ;
            $scope.imageFlag = false;
            localStorage.removeItem('department_response')
            $state.go('hotel-home')

        }).catch(function(){
          alert("Please check internet connection")
        })
      }
    });

  });

}).call(this);
