(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('complaints', {
      url: '/complaints',
      templateUrl: 'app/routes/complaints/complaints.html',
      controller: 'ComplaintsCtrl'

    });
  });

  'use strict';

  angular.module('starter').controller('ComplaintsCtrl', function(Restangular,$scope,Session,$state) {


    function toggleCollapse(obj) {
      for (var key in obj) {
        if (obj[key] && typeof(obj[key]) == 'object') {
          obj[key].collapsed = false;
          toggleCollapse(obj[key])
        }
      }
      return obj
    }

    function Collapse(obj) {
      for (var key in obj) {
        if (obj[key] && typeof(obj[key]) == 'object') {
          obj[key].collapsed = true;
          Collapse(obj[key])
        }
      }
      return obj
    }

    $scope.$on('$ionTreeList:ItemClicked', function(event, item) {


      if(item.depth === 1){
        $scope.pages.map(function(page){
          if(item.id !== page.id){
            Collapse(page)
          }
        })
      }
      item.checked = !item.checked

    });

    $scope.$on('$ionTreeList:LoadComplete', function(event, items) {
      // process 'items'
      //console.log(items);
    });

    $scope.$on("$ionicView.beforeEnter", function() {

      $scope.comments = ""

      var departmentId = Session.read('departmentId')
      $scope.department = Session.read('department')


      $scope.pages = []
      $scope.department.Survey.Pages.map(function(page){
        if(page.type=='content'){
          var xpage = _.pick(page,['id','name']);
          xpage.tree = page.Contents.map(function(content){return _.pick(content,['id','name']);})
          $scope.pages.push(xpage)
        }
        return page
      })
      setTimeout(function(){
        toggleCollapse($scope.pages[0])
      },100)


      $scope.submit = function (comments) {
        var submissionData = {}
        submissionData.TicketContents = []
        $scope.pages.forEach(function(page){
          page.tree.forEach(function(content){
            if(content.checked)
            submissionData.TicketContents.push({pageId:page.id,pageName:page.name,contentId:content.id,contentName:content.name})
          })

        })

        submissionData.userId = Session.read('feedback-user').id;
        submissionData.departmentId = $scope.department.id;
        submissionData.surveyId = $scope.department.Survey.id;
        submissionData.comments = comments;

        Restangular.all('open/tickets').post(submissionData).then(function(){
          alert("Sorry for the inconvenience caused. We are working on the quickest resolution.")
          return $state.go('hotel-home')
        }).catch(function(){
          alert("Error while creating complaint please contact admin@beatleanalytics.com now")
        })
      }

    });


  });

}).call(this);
