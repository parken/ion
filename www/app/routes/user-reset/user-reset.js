(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider.state('user-reset', {
      url: '/user-reset',
      templateUrl: 'app/routes/user-reset/user-reset.html',
      controller: 'UserResetCtrl'

    });
  });

  'use strict';

  angular.module('starter').controller('UserResetCtrl', function($scope, Session, $state, Restangular, $rootScope) {

    $scope.$on("$ionicView.beforeEnter", function() {
      $scope.user = {}
      $scope.otpSentFlag = false;
      $scope.otpVerifiedFlag = false;
      $scope.otpDisable = false;
      $scope.timerFlag = false;
      $scope.buttonText = "Get";
    })




    $scope.sendOtp = function(user){
      Restangular.all('open/users/otpSend')
        .post(user)
        .then(function(otp){
          $scope.otpSentFlag = true
          $scope.userId = otp.userId
          $scope.otpDisable = true;
          $scope.timerFlag = true;
          $rootScope.countdown=60
          setTimeout(function(){
            document.getElementById('otp').getElementsByTagName('timer')[0].start();
          },0)
          setTimeout(function(){
            $scope.$apply(function(){
              $scope.timerFlag = false;
              $scope.otpDisable = false;
              $scope.buttonText = "Resend";
            })
          },60*1000)

        })
        .catch(function(err ){
          alert("Please check internet connection")
        })
    };

    $scope.otpVerify = function(user){
      user.id = $scope.userId
      Restangular.all('open/users/otpVerify')
        .post(user)
        .then(function(response ){
          $scope.otpVerifiedFlag = true
        })
        .catch(function(err_){
          alert("Please check OTP")
        })
    }

    $scope.reset = function(user,passwordResetForm){
      if(user.password === user.rPassword){
        user.id = $scope.userId
        Restangular.all('open/users/reset')
          .post(user)
          .then(function(response) {
            Session.create('feedback-user',response.data);
            return $state.go('hotel-home')
          })
          .catch(function(err){
            alert("Please check internet connection")
          })
      } else {
        passwordResetForm.password.$setValidity('required',false)
        passwordResetForm.rpassword.$setValidity('required',false)
      }

    }
  });

}).call(this);
