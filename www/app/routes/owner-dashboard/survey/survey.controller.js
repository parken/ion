

angular.module('starter')
  .controller('OwnerDashboardSurveyCtrl', function (Restangular, $scope, currentSurvey, $stateParams, $q, $state, ionicDatePicker) {
    const vm = this;

    $scope.$stateParams = $stateParams

    vm.Survey = currentSurvey
    vm.data = {

    };

    $scope.graph ={
      labels: [],
      datasets: [
        {
          label: "Us",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(75,192,192,0.4)",
          borderColor: "rgba(75,192,192,1)",
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: "rgba(75,192,192,1)",
          pointBackgroundColor: "#fff",
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(75,192,192,1)",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [1,2,3]
        },
        {
          label: "Industry",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(75,192,192,0.4)",
          borderColor: "rgba(244,67,54,1)",
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'round',
          pointBorderColor: "rgba(244,67,54,1)",
          pointBackgroundColor: "#fff",
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(244,67,54,1)",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [1,2,3]
        }
      ]
    }

    vm.from = moment().subtract(7,'days').toDate()
    vm.to = new Date()

    vm.createdAt = currentSurvey.createdAt ? new Date(currentSurvey.createdAt): new Date();
    vm.completedAt = currentSurvey.completedAt ? new Date(currentSurvey.completedAt) : new Date();

    var fromObj = {
      callback: function (val) {  //Mandatory
        vm.mFrom = moment(new Date(val)).format('YYYY-MM-DD');
        vm.from = new Date(val);
      },
      from: vm.createdAt, //Optional
      to: vm.completedAt, //Optional
      inputDate: vm.from,      //Optional
    };

    var toObj = {
      callback: function (val) {  //Mandatory
        vm.mTo = moment(new Date(val)).format('YYYY-MM-DD');
        vm.to = new Date(val);
      },
      from: vm.createdAt, //Optional
      to: vm.completedAt, //Optional
      inputDate: vm.to,      //Optional
    };

    $scope.openDatePicker = function(type){
      if(type == 'from'){
        ionicDatePicker.openDatePicker(fromObj);
      } else {
        ionicDatePicker.openDatePicker(toObj);
      }

    };

    function changeCaption(arr){
      arr.forEach(function(item,index){
        $scope.graph.datasets[index].label = item;
      });
    }

    vm.goToPage = function(pageId){
      var details = $stateParams;
      vm.mFrom = moment(vm.from).format('YYYY-MM-DD');
      vm.mTo = moment(vm.to).format('YYYY-MM-DD');

      details.from = vm.mFrom;
      details.to = vm.mTo;

      details.pageId = pageId;
      $state.go('owner-dashboard-survey-page',details);
    }

    function updateGraph(inRep, myRep, duration){
      $scope.graph.labels = duration;
      $scope.graph.data = [
        inRep,
        myRep,
      ];
    }

    function updateGraph2(data, labels){
      $scope.graph.labels = labels;
      $scope.graph.data = data;
    }

    vm.loadedTab = 'averages';

    vm.loadData = function() {
      vm.getData(vm.loadedTab);
    }

    vm.getData = function(type){
      vm.mFrom = moment(vm.from).format('YYYY-MM-DD');
      vm.mTo = moment(vm.to).format('YYYY-MM-DD');

      var details = $stateParams

      details.from = vm.mFrom;
      details.to = vm.mTo;
      if(type !== 'init'){
        vm.loadedTab = type;
      }

      switch(type){
        case 'init': {
          var totalDays = moment(vm.to).diff(vm.from,'days');

          Restangular.one('open/contentResponses','average').get(details).then(function(res){
            vm.data.myAvg = res.myAvg/totalDays;
            vm.data.industryAvg = res.industryAvg/totalDays;
            vm.data.myLowRating = res.myLowRating;
            vm.data.industryLowRating = res.industryLowRating;
            vm.data.averageFeedbacks = res.totalFeedbacks/totalDays;
            vm.data.totalFeedbacks = res.totalFeedbacks;

            // Average Feedback - Industry
            vm.data.avgIndustryFeedbacks = res.totalIndustryFeedbacks/totalDays;
            vm.data.totalIndustryFeedbacks = res.totalIndustryFeedbacks;

            //console.log("avg",res)
          })

          Restangular.one('open/contentResponses','totalTickets').get(details).then(function(res){
            vm.data.averageTickets = res.myTickets / totalDays;
            vm.data.avgIndustryTickets = res.industryTickets / totalDays;
            vm.data.solvedTickets = res.solvedTickets;
            vm.data.myTickets = res.myTickets;
            //console.log("avg",res)
          });


          vm.getData(vm.loadedTab);
          break;
        }
        case 'totalFeedbacks':{
          vm.tF = 6;
          var p = Restangular.all('open/tickets/raised')
            .getList(details);

          var q = Restangular.all('open/feedbacks/raised')
            .getList(details);

          $q.all([p, q]).then(function(res){
            var totalTickets = res[0]
            var totalFeedbacks = res[1]

            updateGraph2(  [
              _.pluck(totalTickets,'CNT'),
              _.pluck(totalFeedbacks,'CNT'),
            ], _.pluck(totalTickets,'DAYID'))
          })
          changeCaption(['Tickets', 'Feedbacks'])
          break;
        }
        case 'solvedTickets':{
          vm.tF = 5;
          var p = Restangular.all('open/tickets/created')
            .getList(details);

          var q = Restangular.all('open/tickets/resolved')
            .getList(details);

          $q.all([p, q]).then(function(res){
            var seriesPendingAvg = res[0]
            var seriesResolvedAvg = res[1]

            var seriesPending = _.pluck(seriesPendingAvg,'CNT');
            var seriesResolved = _.pluck(seriesResolvedAvg,'CNT');

            updateGraph2(  [
              seriesPending,
              seriesResolved,
            ], _.pluck(seriesPending,'DAYID'))
          })
          changeCaption(['Pending','Resolved'])
          break;
        }
        case 'lowRating':{
          vm.tF = 4;
          var p = Restangular.all('open/tickets/avgMyChartData')
            .getList(details);

          var q = Restangular.all('open/tickets/avgIndustryChartData')
            .getList(details);

          $q.all([p, q]).then(function(res){

            var myAvg = res[0]
            var industryAvg = res[1]

            updateGraph2(  [
              _.pluck(industryAvg,'CNT'),
              _.pluck(myAvg,'CNT')
            ], _.pluck(industryAvg,'DAYID'))
          });
          changeCaption(['Industry', 'Us'])


          break;
        }
        case 'tickets':{
          vm.tF = 3;
          var p = Restangular.all('open/contentResponses/avgMyLowRating')
            .getList(details);

          var q = Restangular.all('open/contentResponses/avgIndustryLowRating')
            .getList(details);

          $q.all([p, q]).then(function(res){
            var myAvg = res[0]
            var industryAvg = res[1]


            updateGraph( _.pluck(industryAvg,'CNT'),
              _.pluck(myAvg,'CNT'),_.pluck(industryAvg,'DAYID'))
          })
          changeCaption(['Industry', 'Us'])
          break;
        }
        case 'feedbacks': {
          vm.tF = 2;
          var x = Restangular
            .one('open/feedbacks/reports/departments', $stateParams.departmentId)
            .one('surveys', $stateParams.surveyId)
            .get(details);

          var y = Restangular.all('open/feedbacks/reports')
            .getList(details);

          $q.all([x,y]).then(function(res){
            var myReports = res[0]
            var industryReports = res[1]

            var duration = _.pluck(industryReports,'DAYID')

            updateGraph(
              _.pluck(industryReports,'CNT'),_.pluck(myReports,'CNT'),duration)
            changeCaption(['Industry', 'Us'])
          }).catch(function(err){
            console.log("err",err)
          });

          break;
        }
        case 'averages':
        default: {
          vm.tF = 1;

          var m = Restangular.all('open/contentResponses/avgMyChartData')
            .getList(details);

          var n = Restangular.all('open/contentResponses/avgIndustryChartData')
            .getList(details);

          $q.all([m,n]).then(function(res){
            var myAvg = res[1]
            var industryAvg = res[0]


            updateGraph(
              _.pluck(industryAvg,'CNT'),_.pluck(myAvg,'CNT'),_.pluck(industryAvg,'DAYID'))
          });
          changeCaption(['Industry', 'Us'])
        }
      }

    }

    vm.getData('init');



  });


//
//vm.setFrom = function() {
//  const hour = moment(vm.data.from).get('hour');
//  const minute = moment(vm.data.from).get('minute');
//  vm.data.scheduled_on = moment(vm.data.scheduled_on_date)
//    .set('hour', hour)
//    .set('minute', minute);
//};
//
//vm.setTo = function() {
//  const hour = moment(vm.to).get('hour');
//  const minute = moment(vm.to).get('minute');
//  vm.data.scheduled_on = moment(vm.data.scheduled_on_date)
//    .set('hour', hour)
//    .set('minute', minute);
//};

//Restangular.all('posts').getList().then(function(posts){
//  vm.posts = posts
//}).catch(function(err){
//  console.log(err)
//  alert("Error while fetching posts from server ")
//})
