(function() {
  'use strict';
  angular.module('starter').config(function($stateProvider) {
    $stateProvider
      .state('owner-dashboard', {
        url: '/owner-dashboard',
        templateUrl: 'app/routes/owner-dashboard/owner-dashboard.html',
        controller: 'OwnerDashboardCtrl',
        controllerAs: 'OwnerDashboard'
      })
      .state('owner-dashboard-survey', {
        url: '/owner-dashboard/chains/:chainId/hotels/:hotelId/departments/:departmentId/surveys/:surveyId',
        templateUrl: 'app/routes/owner-dashboard/survey/survey.html',
        controller: 'OwnerDashboardSurveyCtrl',
        controllerAs: 'OwnerDashboardSurvey',
        resolve: {
          currentSurvey: function($stateParams,Restangular){
            return Restangular
              .one('open/surveys',$stateParams.surveyId)
              .get()
              .catch(function(err){
                console.log("Error while getting survey",err)
              })
          }
        }
      })

      .state('owner-dashboard-survey-page', {
        url: '/owner-dashboard/chains/:chainId/hotels/:hotelId/departments/:departmentId/surveys/:surveyId/pages/:pageId?from,to',
        templateUrl: 'app/routes/owner-dashboard/page/list/list.html',
        controller: 'OwnerDashboardPageListCtrl',
        controllerAs: 'OwnerDashboardPageList',
        resolve: {
          currentSurvey: function($stateParams,Restangular){
            return Restangular
              .one('open/surveys',$stateParams.surveyId)
              .get()
              .catch(function(err){
                console.log("Error while getting survey",err)
              })
          }
        }
      })
      .state('owner-dashboard-survey-page-content', {
        url: '/owner-dashboard/chains/:chainId/hotels/:hotelId/departments/:departmentId/surveys/:surveyId/pages/:pageId/contents?from,to',
        templateUrl: 'app/routes/owner-dashboard/page/page.html',
        controller: 'OwnerDashboardPageCtrl',
        controllerAs: 'OwnerDashboardPage',
        resolve: {
          currentSurvey: function($stateParams,Restangular){
            return Restangular
              .one('open/surveys',$stateParams.surveyId)
              .get()
              .catch(function(err){
                console.log("Error while getting survey",err)
              })
          }
        }
      });
  });

  'use strict';

  angular.module('starter')
    .controller('OwnerDashboardCtrl', function (Restangular,$scope, Session) {

      var vm = this;

      $scope.$on("$ionicView.beforeEnter", function() {
        vm.chains = Session.read('department');
        //Restangular.one('open/departments',localStorage.departmentId).all('lineManagerData').getList().then(function(chains){
        //  vm.chains= chains
        //}).catch(function(err){
        //  console.log("Error while getting chains",err)
        //  return alert("Error while getting surveys")
        //})

      })



    });

}).call(this);
