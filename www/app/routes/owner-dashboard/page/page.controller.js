
angular.module('starter')
  .controller('OwnerDashboardPageCtrl', function (Restangular, $scope, currentSurvey, $stateParams, $q, $state, $filter, ionicDatePicker) {
    const vm = this;

    $scope.$stateParams = $stateParams

    vm.data = { };
    $scope.graph ={
      labels: [],
      datasets: [
        {
          label: "Us",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(75,192,192,0.4)",
          borderColor: "rgba(75,192,192,1)",
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: "rgba(75,192,192,1)",
          pointBackgroundColor: "#fff",
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(75,192,192,1)",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [1,2,3]
        },
        {
          label: "Industry",
          fill: false,
          lineTension: 0.1,
          backgroundColor: "rgba(75,192,192,0.4)",
          borderColor: "rgba(244,67,54,1)",
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'round',
          pointBorderColor: "rgba(244,67,54,1)",
          pointBackgroundColor: "#fff",
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgba(244,67,54,1)",
          pointHoverBorderColor: "rgba(220,220,220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: [1,2,3]
        }
      ]
    }

    vm.from = moment().subtract(7,'days').toDate()
    vm.to = new Date();

    $scope.ui = {
      tF: 0
    }

    vm.createdAt = currentSurvey.createdAt ? new Date(currentSurvey.createdAt): new Date();
    vm.completedAt = currentSurvey.completedAt ? new Date(currentSurvey.completedAt) : new Date();

    var fromObj = {
      callback: function (val) {  //Mandatory
        vm.mFrom = moment(new Date(val)).format('YYYY-MM-DD');
        vm.from = new Date(val);
      },
      from: vm.createdAt, //Optional
      to: vm.completedAt, //Optional
      inputDate: vm.from,      //Optional
    };

    var toObj = {
      callback: function (val) {  //Mandatory
        vm.mTo = moment(new Date(val)).format('YYYY-MM-DD');
        vm.from = new Date(val);
      },
      from: vm.createdAt, //Optional
      to: vm.completedAt, //Optional
      inputDate: vm.to,      //Optional
    };

    $scope.openDatePicker = function(type){
      if(type == 'from'){
        ionicDatePicker.openDatePicker(fromObj);
      } else {
        ionicDatePicker.openDatePicker(toObj);
      }

    };



    function updateGraph2(data, labels){
      $scope.graph.labels = labels ;
      data = data.map(function(d){
        return d.map(function(value){
          return $filter('number')(value,1)
        })
      })
      $scope.graph.data = data;
      console.log($scope.graph)
    }


    vm.getPageHref = function(id){
      var details = $stateParams;
      vm.mFrom = moment(vm.from).format('YYYY-MM-DD');
      vm.mTo = moment(vm.to).format('YYYY-MM-DD');

      details.from = vm.mFrom;
      details.to = vm.mTo;

      details.pageId = id;

      return $state.go('owner-dashboard-survey-page-content',details,{reload:true});
    }

    /* Detailed Report */
    $scope.Survey = currentSurvey

    var contentPages = _.filter(currentSurvey.Pages, {type:'content'})
    var contentPageIds = _.pluck(contentPages,'id')

    if(!$stateParams.pageId) return

    var currentPageIndex = contentPageIds.indexOf(parseInt($stateParams.pageId))



    $scope.currentPage = currentSurvey.Pages[currentPageIndex];
    $scope.totalItems = $scope.currentPage.Contents.length;

    var details = $stateParams;
    vm.mFrom = moment(vm.from).format('YYYY-MM-DD');
    vm.mTo = moment(vm.to).format('YYYY-MM-DD');
    details.from = vm.mFrom;
    details.to = vm.mTo;

    Restangular.one('open/contentResponses','detailedReports').get(details).then(function(res){
      vm.data.myContentAvg = res.myContentAvg;
      vm.data.industryContentAvg = res.industryContentAvg;
    })

    var lastContentId, lastIndex;
    $scope.changeTab = function(contentId, $index){
      lastContentId = contentId || lastContentId;
      lastIndex = $index || lastIndex;

      var details = $stateParams;
      details.contentId = lastContentId
      vm.mFrom = moment(vm.from).format('YYYY-MM-DD');
      vm.mTo = moment(vm.to).format('YYYY-MM-DD');
      details.from = vm.mFrom;
      details.to = vm.mTo;

      $scope.ui.tF = lastIndex;
      var p = Restangular.all('open/contentResponses/avgMyContents')
        .getList(details);

      var q = Restangular.all('open/contentResponses/avgIndustryContents')
        .getList(details);

      $q.all([p, q]).then(function(res){
        var avgMyContents = res[0]
        var avgIndustryContents = res[1]

        updateGraph2(  [
          _.pluck(avgMyContents,'CNT'),
          _.pluck(avgIndustryContents,'CNT'),
        ], _.pluck(avgMyContents,'DAYID'))
      })
      $scope.graph.series = [ 'Us', 'Industry'];
    }

    $scope.changeTab($scope.currentPage.Contents[$scope.ui.tF].id,$scope.ui.tF )
  });

