
angular.module('starter').filter('TicketContents', function ($sce) {
  return function (input) {

    var output = {};
    input.forEach(function(pageContent){
      if(!output[pageContent.pageName]) output[pageContent.pageName] = []
      output[pageContent.pageName].push(pageContent.contentName)
    })
    var str = "";
    for(var i in output){
      str += '<strong>' +i + '</strong>'+":"+ output[i].join(',')+"<br>";
    }
    return $sce.trustAsHtml(str || 'Nothing ')
  };
})
