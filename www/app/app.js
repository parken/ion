// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','restangular','chart.js', 'ion-tree-list','timer','ionic-datepicker'])

.run(function($ionicPlatform, $rootScope,Session,$state) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

})

.config(function($stateProvider, $urlRouterProvider,RestangularProvider, ionicDatePickerProvider) {
  var datePickerObj = {
    inputDate: new Date(),
    setLabel: 'Set',
    todayLabel: 'Today',
    closeLabel: 'Close',
    mondayFirst: false,
    weeksList: ["S", "M", "T", "W", "T", "F", "S"],
    monthsList: ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"],
    templateType: 'popup',
    from: new Date(2012, 8, 1),
    to: new Date(2018, 8, 1),
    showTodayButton: true,
    dateFormat: 'dd MMMM yyyy',
    closeOnSelect: false,
    disableWeekdays: [6]
  };
  ionicDatePickerProvider.configDatePicker(datePickerObj);
  RestangularProvider.setBaseUrl('https://api.beatleanalytics.com/api');
  // var PBB_API = 'http://localhost:9000/api';
  //if (location.host === 'mobile.beatleanalytics.com' || location.host === '') {
  //  PBB_API = 'https://api.beatleanalytics.com/api';
  //}
  //
  // RestangularProvider.setBaseUrl(PBB_API);

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/department-login');

}).directive('focusMe', function($timeout, $parse) {
  return {
    //scope: true,   // optionally create a child scope
    link: function(scope, element, attrs) {
      var model = $parse(attrs.focusMe);
      scope.$watch(model, function(value) {
        if(value === true) {
          $timeout(function() {
            element[0].focus();
          });
        }
      });
      // to address @blesh's comment, set attribute value to 'false'
      // on blur event:
      element.bind('blur', function() {
        //scope.$apply(model.assign(scope, false));
      });
    }
  };
}) .directive('sbLoad', ['$parse', function ($parse) {
  return {
    restrict: 'A',
    link: function (scope, elem, attrs) {
      var fn = $parse(attrs.sbLoad);
      elem.on('load', function (event) {
        var canvas = document.createElement('canvas'),
        context = canvas.getContext('2d');
        context.drawImage(event.target, 0, 0,event.target.width/10,event.target.height/10);
        //console.log("s",canvas.toDataURL());
        scope.$apply(function() {
          fn(scope, { $event: canvas.toDataURL() });
        });
      });
    }
  };
}]);
